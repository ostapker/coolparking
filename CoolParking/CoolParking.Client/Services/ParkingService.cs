﻿using CoolParking.Client.Interfaces;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace CoolParking.Client.Services
{
    public class ParkingService : IParkingService
    {
        private HttpClient _client;
        private string _baseUrl = "";

        public ParkingService(string baseUrl, HttpClient client)
        {
            _baseUrl = baseUrl;
            _client = client;
        }

        public async Task<decimal> GetBalance()
        {
            var balance = await _client.GetStringAsync($"{_baseUrl}/api/parking/balance");
            return  JsonConvert.DeserializeObject<decimal>(balance);
        }

        public async Task<int> GetCapacity()
        {
            var capcity = await _client.GetStringAsync($"{_baseUrl}/api/parking/capacity");
            return JsonConvert.DeserializeObject<int>(capcity);
        }

        public async Task<int> GetFreePlaces()
        {
            var freePlaces = await _client.GetStringAsync($"{_baseUrl}/api/parking/freePlaces");
            return JsonConvert.DeserializeObject<int>(freePlaces);
        }
    }
}
