﻿using CoolParking.Client.Interfaces;
using CoolParking.Client.ViewModels;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace CoolParking.Client.Services
{
    public class TransactionsService : ITransactionsService
    {
        private HttpClient _client;
        private string _baseUrl = "";

        public TransactionsService(string baseUrl, HttpClient client)
        {
            _baseUrl = baseUrl;
            _client = client;
        }

        public async Task<IEnumerable<TransactionViewModel>> GetLastTransactions()
        {
            var transactions = await _client.GetStringAsync($"{_baseUrl}/api/transactions/last");
            return JsonConvert.DeserializeObject<IEnumerable<TransactionViewModel>>(transactions);
        }


        public async Task<string> GetAllTransactions()
        {
            var response = await _client.GetAsync($"{_baseUrl}/api/transactions/all");
            if (!response.IsSuccessStatusCode)
            {
                throw new Exception($"{response.StatusCode}: {await response.Content.ReadAsStringAsync()}");
            }

            return await response.Content.ReadAsStringAsync();
        }

        public async Task<VehicleViewModel> TopUpVehicle(string id, decimal Sum)
        {
            var response = await _client.PutAsJsonAsync($"{_baseUrl}/api/transactions/topUpVehicle", new { id, Sum });
            if (response.StatusCode != System.Net.HttpStatusCode.OK)
            {
                throw new Exception($"{response.StatusCode}: {await response.Content.ReadAsStringAsync()}");
            }
            return await response.Content.ReadAsAsync<VehicleViewModel>();
        }
    }
}
