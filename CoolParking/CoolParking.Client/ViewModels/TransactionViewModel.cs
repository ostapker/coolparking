﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace CoolParking.Client.ViewModels
{
    public class TransactionViewModel
    {
        [JsonProperty("sum")]
        public decimal Sum { get; set; }

        [JsonProperty("vehicleId")]
        public string VehicleId { get; set; }

        [JsonProperty("time")]
        public DateTime TransactionDate { get; set; }

        public override string ToString()
        {
            return string.Format($"{TransactionDate.ToString("dd/MM/yyyy HH:mm:ss")}: {Sum.ToString("0.00")} money withdrawn from vehicle with Id='{VehicleId}'.");
        }
    }
}
