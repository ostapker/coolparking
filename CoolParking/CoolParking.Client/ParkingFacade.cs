﻿using CoolParking.BL.Models;
using CoolParking.Client.Interfaces;
using CoolParking.Client.Services;
using CoolParking.Client.ViewModels;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace CoolParking.Client
{
    public class ParkingFacade
    {
        private readonly string baseUrl = "http://localhost:59461";
        private readonly IParkingService parkingService;
        private readonly IVehiclesService vehiclesService;
        private readonly ITransactionsService transactionsService;
        private readonly HttpClient httpClient;

        public ParkingFacade()
        {
            httpClient = new HttpClient();
            parkingService = new ParkingService(baseUrl, httpClient);
            vehiclesService = new VehiclesService(baseUrl, httpClient);
            transactionsService = new TransactionsService(baseUrl, httpClient);
        }

        public async Task Start()
        {
            while (true)
            {
                Console.WriteLine("Choose command:");
                Console.WriteLine("1 - print parking balance");
                Console.WriteLine("2 - print parking capacity");
                Console.WriteLine("3 - print parking free places");
                Console.WriteLine("4 - print vehicles list");
                Console.WriteLine("5 - print vehicle by id");
                Console.WriteLine("6 - add vehicle");
                Console.WriteLine("7 - remove vehicle");
                Console.WriteLine("8 - print last transactions");
                Console.WriteLine("9 - print all transactions");
                Console.WriteLine("10 - topUp vehicle balance");
                Console.WriteLine("0 - exit");
                var commandString = Console.ReadLine();
                Console.WriteLine();
                try
                {
                    int command = int.Parse(commandString);

                    switch (command)
                    {
                        case 0:
                            return;
                        case 1:
                            await PrintParkingBalance();
                            break;
                        case 2:
                            await PrintParkingCapacity();
                            break;
                        case 3:
                            await PrintParkingFreePlaces();
                            break;
                        case 4:
                            await PrintVehicles();
                            break;
                        case 5:
                            await PrintVehicleById();
                            break;
                        case 6:
                            await AddVehicle();
                            break;
                        case 7:
                            await RemoveVehicle();
                            break;
                        case 8:
                            await PrintLastTransactions();
                            break;
                        case 9:
                            await PrintAllTransactions();
                            break;
                        case 10:
                            await TopUpVehicle();
                            break;
                    }

                }
                catch (FormatException)
                {
                    Console.WriteLine($"Incorrect command");
                }
                Console.WriteLine();
            }
        }

        public async Task PrintParkingBalance()
        {
            Console.WriteLine($"Balance = {await parkingService.GetBalance()}");
        }

        public async Task PrintParkingCapacity()
        {
            Console.WriteLine($"Capacity = {await parkingService.GetCapacity()}");
        }

        public async Task PrintParkingFreePlaces()
        {
            Console.WriteLine($"Capacity = {await parkingService.GetFreePlaces()}");
        }

        public async Task PrintVehicles()
        {
            Console.WriteLine("Vehicles on parking:");
            foreach(var vehicle in await vehiclesService.GetVehicles())
            {
                Console.WriteLine(vehicle.ToString());
            }
        }

        public async Task PrintVehicleById()
        {
            Console.WriteLine("Enter vehicle id:");
            var id = Console.ReadLine();
            try
            {
                var vehicle = await vehiclesService.GetVehicle(id);
                Console.WriteLine(vehicle.ToString());
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }

        public async Task AddVehicle()
        {
            Console.WriteLine("Enter vehicle id:");
            var Id = Console.ReadLine();
            Console.WriteLine("Specify Vyhicle type:");
            Console.WriteLine("\t1 - {0}", VehicleType.PassengerCar.ToString());
            Console.WriteLine("\t2 - {0}", VehicleType.Truck.ToString());
            Console.WriteLine("\t3 - {0}", VehicleType.Bus.ToString());
            Console.WriteLine("\t4 - {0}", VehicleType.Motorcycle.ToString());
            var vehicleTypeString = Console.ReadLine();
            Console.WriteLine("Enter Initial vehicle balance:");
            var balanceString = Console.ReadLine();
            try
            {
                int vehicleType = int.Parse(vehicleTypeString) - 1;
                decimal Balance = decimal.Parse(balanceString);

                var result = await vehiclesService.PostVehicle(
                    new VehicleViewModel { Id=Id, VehicleType=(VehicleType)vehicleType, Balance=Balance});

                Console.WriteLine(result.ToString());
            }
            catch (FormatException e)
            {
                Console.WriteLine(e.Message);
            }
            catch(Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }

        public async Task RemoveVehicle()
        {
            Console.WriteLine("Enter vehicle id:");
            var Id = Console.ReadLine();
            try
            {
                await vehiclesService.DeleteVehicle(Id);
                Console.WriteLine("Vehicle removed from parking");
            }
            catch(Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }

        public async Task PrintLastTransactions()
        {
            Console.WriteLine("Last transactions:");
            foreach (var transaction in await transactionsService.GetLastTransactions())
            {
                Console.WriteLine(transaction.ToString());
            }
        }

        public async Task PrintAllTransactions()
        {
            try
            {
                Console.WriteLine("Logged transactions:");
                Console.WriteLine(await transactionsService.GetAllTransactions());
            }
            catch(Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }

        public async Task TopUpVehicle()
        {
            Console.WriteLine("Enter vehicle id:");
            var id = Console.ReadLine();
            Console.WriteLine("Enter sum to TopUp:");
            var sumString = Console.ReadLine();
            try
            {
                decimal sum = decimal.Parse(sumString);
                var result = await transactionsService.TopUpVehicle(id, sum);
                Console.WriteLine(result.ToString());
            }
            catch (FormatException e)
            {
                Console.WriteLine(e.Message);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }
    }
}
