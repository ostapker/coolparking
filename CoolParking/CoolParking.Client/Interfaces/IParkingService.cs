﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace CoolParking.Client.Interfaces
{
    public interface IParkingService
    {
        Task<decimal> GetBalance();

        Task<int> GetCapacity();

        Task<int> GetFreePlaces();
    }
}
