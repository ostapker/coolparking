﻿using CoolParking.Client.ViewModels;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace CoolParking.Client.Interfaces
{
    public interface ITransactionsService
    {
        Task<IEnumerable<TransactionViewModel>> GetLastTransactions();

        Task<string> GetAllTransactions();

        Task<VehicleViewModel> TopUpVehicle(string id, decimal Sum);
    }
}
