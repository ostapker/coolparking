﻿using CoolParking.Client.ViewModels;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace CoolParking.Client.Interfaces
{
    public interface IVehiclesService
    {
        Task<IEnumerable<VehicleViewModel>> GetVehicles();

        Task<VehicleViewModel> GetVehicle(string id);

        Task<VehicleViewModel> PostVehicle(VehicleViewModel vehicle);

        Task DeleteVehicle(string id);
    }
}
