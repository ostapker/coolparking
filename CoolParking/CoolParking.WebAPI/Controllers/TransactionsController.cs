﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CoolParking.BL.Interfaces;
using CoolParking.BL.Models;
using CoolParking.WebAPI.ViewModels;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace CoolParking.WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TransactionsController : ControllerBase
    {
        private readonly IParkingService parkingService;
        public TransactionsController(IParkingService parkingService)
        {
            this.parkingService = parkingService;
        }

        [HttpGet("last")]
        public ActionResult<IEnumerable<TransactionInfo>> GetLastTransactions()
        {
            return Ok(parkingService.GetLastParkingTransactions());
        }

        [HttpGet("all")]
        public ActionResult<string> GetAllTransactions()
        {
            try
            {
                return Ok(parkingService.ReadFromLog());
            }
            catch (InvalidOperationException e)
            {
                return NotFound(e.Message);
            }
        }

        [HttpPut("topUpVehicle")]
        public ActionResult<Vehicle> TopUpVehicle(VehicleTopUpViewModel vehicleTopUpViewModel)
        {
            try
            {
                parkingService.TopUpVehicle(vehicleTopUpViewModel.Id, vehicleTopUpViewModel.Sum);
                return Ok(parkingService.FindById(vehicleTopUpViewModel.Id));
            }
            catch (NotSupportedException)
            {
                return BadRequest("Invalid request body");
            }
            catch(ArgumentException e)
            {
                return BadRequest(e.Message);
            }
            catch (NullReferenceException e)
            {
                return NotFound(e.Message);
            }
        }
    }
}