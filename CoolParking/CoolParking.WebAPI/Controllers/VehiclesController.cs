﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using CoolParking.BL.Interfaces;
using CoolParking.BL.Models;
using CoolParking.WebAPI.ViewModels;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace CoolParking.WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class VehiclesController : ControllerBase
    {
        private readonly IParkingService parkingService;

        public VehiclesController(IParkingService parkingService)
        {
            this.parkingService = parkingService;
        }

        [HttpGet]
        public ActionResult<IEnumerable<Vehicle>> GetVehicles()
        {
            return Ok(parkingService.GetVehicles());
        }

        [HttpGet("{id}")]
        public ActionResult<Vehicle> GetVehicle(string id)
        {
            try
            {
                var vehicle = parkingService.GetVehicles().Where(v => v.Id == id).FirstOrDefault();
                return Ok(parkingService.FindById(id));
            }
            catch(ArgumentException e)
            {
                return BadRequest(e.Message);
            }
            catch (NullReferenceException e)
            {
                return NotFound(e.Message);
            }
        }

        [HttpPost]
        public ActionResult<Vehicle> PostVehicle(Vehicle vehicle)
        {
            try
            {
                parkingService.AddVehicle(new Vehicle(vehicle.Id, vehicle.VehicleType, vehicle.Balance));
                return Created($"api/vehicles/{vehicle.Id}", vehicle);
            }
            catch(NotSupportedException)
            {
                return BadRequest("Invalid request body");
            }
            catch(ArgumentException e)
            {
                return BadRequest(e.Message);
            }
        }

        [HttpDelete("{id}")]
        public ActionResult DeleteVehicle(string id)
        {
            try
            {
                parkingService.RemoveVehicle(id);
                return NoContent();
            }
            catch(ArgumentException e)
            {
                return BadRequest(e.Message);
            }
            catch (NullReferenceException e)
            {
                return NotFound(e.Message);
            }
            catch(InvalidOperationException e)
            {
                return BadRequest(e.Message);
            }
        }

    }
}